'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');
const Env = use('Env');

Route.get('/', ({response}) => {
	return response.redirect(Env.get("FRONT_URL"))
});

Route.group(() => {
	Route.get('list/', 'BeatController.list');
	Route.get('admin/list', 'BeatController.admin_list').middleware(['admin']);
	Route.post('admin/insert', 'BeatController.insert').middleware(['admin']);
	Route.post('admin/delete', 'BeatController.delete').middleware(['admin']);
	Route.post('admin/file/:id', 'BeatController.file_upload').middleware(['admin']);
	Route.get(':uuid', 'BeatController.download');//.middleware(['auth']);
}).prefix('beats/');

Route.group(() => {
	Route.post('login/', 'UserController.login');
	Route.post('register/', 'UserController.register');
	Route.get('orders/', 'UserController.orders').middleware(['auth']);
	Route.get('logged', () => {
		return {status: true}
	}).middleware(['auth']);
}).prefix('accounts/');

Route.group(() => {
	
	Route.post('insert/', 'PromotionController.insert');
	Route.post('delete/', 'PromotionController.delete');
	Route.get('list/', 'PromotionController.list');
	
	
}).prefix('promotions/').middleware(['admin']);

Route.post('checkpayment/', 'OrderController.checkpayment').middleware(['auth']);

Route.post('buy/', 'OrderController.buy').middleware(['auth']);
