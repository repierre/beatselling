'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class OrdersSchema extends Schema {
	up() {
		this.table('orders', (table) => {
			table.integer('user_id').unsigned().references('users.id');
		})
	}
	
	down() {
		this.table('orders', (table) => {
			table.delete('user_id');
		})
	}
}

module.exports = OrdersSchema;
