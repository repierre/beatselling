'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BeatsSchema extends Schema {
	up() {
		this.table('beats', (table) => {
			table.integer('duration');
		})
	}
	
	down() {
		this.table('beats', (table) => {
			table.dropColumn('duration');
		})
	}
}

module.exports = BeatsSchema
