'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PromotionsSchema extends Schema {
	up() {
		this.table('promotions', (table) => {
			table.string('code');
		})
	}
	
	down() {
		this.table('promotions', (table) => {
			table.dropColumn('code');
		})
	}
}

module.exports = PromotionsSchema
