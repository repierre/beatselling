'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class CreateOrderSchema extends Schema {
	up() {
		this.create('orders', (table) => {
			table.increments();
			table.string('order_id');
			table.integer('order_price');
			table.boolean('status');
			table.timestamps();
		})
	}
	
	down() {
		this.drop('orders')
	}
}

module.exports = CreateOrderSchema;
