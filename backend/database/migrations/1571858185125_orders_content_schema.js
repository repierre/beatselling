'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class OrdersContentSchema extends Schema {
	up() {
		this.create('orders_contents', (table) => {
			table.increments();
			table.integer('order_id').unsigned().references('orders.id');
			table.integer('beat_id').unsigned().references('beats.id');
			table.timestamps();
		})
	}
	
	down() {
		this.drop('orders_contents')
	}
}

module.exports = OrdersContentSchema;
