'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class BeatCreationSchema extends Schema {
  up () {
    this.create('beats', (table) => {
      table.increments();
      table.string("name");
      table.integer("price");
      table.timestamps();
    })
  }

  down () {
    this.drop('beats');
  }
}

module.exports = BeatCreationSchema;
