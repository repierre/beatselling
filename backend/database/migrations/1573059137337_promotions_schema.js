'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class PromotionsSchema extends Schema {
	up() {
		this.create('promotions', (table) => {
			table.increments();
			table.integer('type');
			table.integer('value');
			table.integer('end');
			table.integer('use');
			table.timestamp('date');
			table.timestamps();
		})
	}
	
	down() {
		this.drop('promotions')
	}
}

module.exports = PromotionsSchema;
