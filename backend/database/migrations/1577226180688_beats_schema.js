'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class BeatsSchema extends Schema {
	up() {
		this.table('beats', (table) => {
			table.integer('bpm');
			table.string('author');
			table.string('file');
			table.dropColumn('uuid');
		})
	}
	
	down() {
		this.table('beats', (table) => {
			table.dropColumn('author');
			table.dropColumn('bpm');
			table.dropColumn('file');
		})
	}
}

module.exports = BeatsSchema;
