'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class BeatsSchema extends Schema {
	up() {
		this.table('beats', (table) => {
			table.string('ext');
		})
	}
	
	down() {
		this.table('beats', (table) => {
			table.dropColumn('ext');
		})
	}
}

module.exports = BeatsSchema;
