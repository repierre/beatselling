'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class AddOrderToBeatsSchema extends Schema {
	up() {
		this.table('beats', (table) => {
			table.integer('order').unsigned().references('orders.id');
		})
	}
	
	down() {
		this.table('beats', (table) => {
			table.delete('order');
		})
	}
}

module.exports = AddOrderToBeatsSchema;
