'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Order extends Model {
	
	beats() {
		return this.manyThrough('App/Models/OrdersContent', 'beat');
	}
	
	content() {
		return this.hasMany('App/Models/OrdersContent');
	}

	user() {
		return this.belongsTo('App/Models/User', 'user_id', 'id')
	}
	
}

module.exports = Order;
