'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Beat extends Model {
	
	static get visible() {
		return ['id', 'uuid', 'name', 'price', 'duration', 'bpm', 'author', 'ext']
	}
	
}

module.exports = Beat;
