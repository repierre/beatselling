'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Promotion extends Model {
	
	static get visible() {
		return ['type', 'value', 'end', 'use', 'date', 'code'];
	}

}

module.exports = Promotion;
