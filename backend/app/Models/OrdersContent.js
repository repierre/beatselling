'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class OrdersContent extends Model {
	
	beat() {
		return this.belongsTo('App/Models/Beat');
	}
	
	order() {
		return this.belongsTo('App/Models/Order');
	}
	
}

module.exports = OrdersContent;
