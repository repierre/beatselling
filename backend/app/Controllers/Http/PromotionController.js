'use strict';

const Promotion = use('App/Models/Promotion');

class PromotionController {
	
	async list({response}) {
		let promos = await Promotion.all();
		promos = promos.rows;
		const ret = [];
		promos.forEach(promo => {
			const obj = {};
			obj.code = promo.code;
			//obj.type = promo.type;
			//obj.value = promo.value;
			obj.type = promo.value + (promo.type === 1 ? ' €' : ' %');
			//obj.end = promo.end;
			if (promo.end === 1) obj.end = promo.use;
			else obj.end = promo.date;
			ret.push(obj);
		});
		return response.send(ret);
	}
	
	async delete({request}) {
		const data = JSON.parse(request.raw());
		const code = data.code;
		
		
		try {
			const promo = await Promotion.findBy('code', code);
			await promo.delete();
			return {
				status: true,
			}
		} catch (e) {
			return {
				status: false,
				error: {
					code: 'ER_NO_ENTRY',
				},
			}
		}
	}
	
	async insert({request}) {
		
		const data = JSON.parse(request.raw());
		const type = data.type;
		const end = data.end;
		
		const promo = new Promotion();
		
		promo.code = data.code;
		promo.type = type.type;
		promo.value = type.value;
		
		promo.end = end.type;
		console.log(end);
		if (end.type === 1) promo.use = end.value;
		else if (end.type === 2) promo.date = end.value;
		
		try {
			await promo.save();
			return {
				status: true,
				promotion: promo,
			}
		} catch (e) {
			return {
				status: false,
				error: e
			}
		}
		
	}
	
}

module.exports = PromotionController;
