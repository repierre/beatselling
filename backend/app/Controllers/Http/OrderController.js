'use strict';
const Beat = use('App/Models/Beat');
const OrdersContent = use('App/Models/OrdersContent');
const fetch = require('node-fetch');
const Order = use('App/Models/Order');
const Mailer = require('nodemailer');


const CLIENT_ID = "AVKi3NguVuCpjWya-ysFjBDGrhSpWlDqgdarR1_lX0XYcfQqREEOJ7VZciOfHDw1B1UBbjj-HIYFTWKC";
const CLIENT_SECRET = "ELyGdLIgyqPsevi_-fatqIwoTIoSKPJR8FQncBy8wUR6zB_644vQPrLBmQKwO2GICXX6tkoKB_iVCVCe";
const TOKEN = new Buffer(CLIENT_ID + ':' + CLIENT_SECRET).toString('base64');

class OrderController {
	
	async buy({request, response, auth}) {
		const data = request.post().tracks;
		const ids = [];
		data.forEach(track => ids.push(track.id));
		
		let items = await Beat.query().whereIn('id', ids).fetch();
		items = items.rows;
		
		const price = items.reduce((total, item) => {
			return total + item.price
		}, 0);
		
		let order = new Order();
		order.order_price = price;
		order.status = false;
		const body = JSON.stringify({
			intent: 'CAPTURE',
			purchase_units: [
				{
					amount: {
						value: price,
						currency_code: 'EUR',
					}
				}
			]
		});
		
		const orderId = await fetch('https://api.sandbox.paypal.com/v2/checkout/orders', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'Basic ' + TOKEN,
			},
			body: body
		}).then(data => data.json()).then(json => {
			return json.id;
		});
		
		order.order_id = orderId;
		
		await order.save();
		
		for (let item of items) {
			let ctc = await order.content().create({});
			ctc.save();
			ctc.beat().associate(item);
		}
		
		await order.user().associate(auth.user);
		
		
		return {
			orderID: orderId,
		}
	}
	
	async checkpayment({auth, request}) {
		
		const data = request.post().data;
		const payment = data.payment;
		
		const state = await fetch('https://api.sandbox.paypal.com/v2/checkout/orders/' + payment, {
			headers: {
				Accept: 'application/json',
				Authorization: 'Basic ' + TOKEN,
			}
		}).then(data => data.json());
		
		const price = state.purchase_units[0].amount.value;
		const status = state.status;
		
		if (status === 'COMPLETED') {
			
			const order = await Order.findBy('order_id', payment);
			
			
			const usr = await order.user().fetch();
			let beats = await order.beats().fetch();
			beats = beats.rows;
			
			if (usr.email !== auth.user.email) {
				order.beats().delete();
				order.delete();
				return {
					status: false,
					message: 'This command is not registered to your account.',
				}
			}
			
			if (parseFloat(order.order_price) === parseFloat(price)) {
				//COMMANDE OK
				order.status = 1;
				order.save();
				
				return {
					status: true,
					message: 'Your command has been validated, you will find the beats in your profile.'
				}
			} else {
				//PAS BON !
				order.beats().delete();
				order.delete();
				return {
					status: false,
					message: 'The price doesn\'t match.'
				}
			}
		}
		
		return {
			status: false,
			message: 'Command not validated yet.'
		};
	}
	
}

module.exports = OrderController;
