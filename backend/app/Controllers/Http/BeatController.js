'use strict';

const Beat = use("App/Models/Beat");
const Helpers = use('Helpers');
const Order = use('App/Models/Order');
const fs = require('fs-extra');
const uuid = require('uuid/v4');
const mime = require('mime-types');
const ffmpeg = require('fluent-ffmpeg');

class BeatController {
	
	async list({response, auth}) {
		const user = auth.user;
		if (user !== null) {
			
			const orders = await user.orders().fetch();
			
			const beat_exclude = [];
			
			for (let order of orders.rows) {
				if (order.status !== 1) continue;
				
				let beats = await order.beats().fetch();
				beats = beats.rows;
				for (let beat of beats) beat_exclude.push(beat.id);
			}
			
			let beats = await Beat.query().whereNotIn('id', beat_exclude).fetch();
			beats = beats.rows;
			if (beats === undefined) beats = [];
			response.send(beats);
			
		} else {
			let beats = await Beat.all();
			response.send(beats);
		}
	}
	
	async admin_list() {
		let beats = await Beat.all();
		return {
			status: true,
			beats
		}
	}
	
	async insert({request, response}) {
		const beat = new Beat();
		const data = JSON.parse(request.raw());
		beat.name = data.name;
		beat.price = data.price;
		beat.duration = data.duration;
		beat.bpm = data.bpm;
		beat.author = data.author;
		beat.uuid = uuid();
		
		try {
			await beat.save();
			response.send({
				status: true,
				uuid: beat.uuid,
			});
		} catch (e) {
			let error = {code: e.code};
			if (e.code === 'ER_DUP_ENTRY') {
				let field = e.sqlMessage.match(/(?<=key ')[A-Za-z0-9]+/g)[0];
				error = {
					code: 'ER_ALREADY_EXIST',
					field
				}
			}
			response.send({
				status: false,
				error
			})
		}
	}
	
	async delete({request, response}) {
		const data = JSON.parse(request.raw());
		const uuid = data.uuid;
		const beat = await Beat.findBy('uuid', uuid);
		if (beat == null) {
			response.send({
				status: false,
				error: {
					code: 'ER_NO_BEAT'
				}
			})
		} else {
			await beat.delete();
			response.send({
				status: true
			})
		}
	}
	
	async file_upload({request, response}) {
		const uuid = request.all().uuid;
		const file = request.file('file');
		const beat = await Beat.findBy('uuid', uuid);
		if (beat == null) {
			response.send({
				status: false,
				error: {
					code: 'ER_NO_BEAT'
				}
			})
		} else {
			await file.move(Helpers.resourcesPath('beats'), {
				name: beat.uuid + '.' + file.extname,
				overwrite: true
			});
			if (!file.moved()) {
				return response.send({
					status: false,
				})
			}
			beat.ext = file.extname;
			await beat.save();
			
			const path = Helpers.resourcesPath('beats') + '/' + beat.uuid + '.' + beat.ext;
			const trimPath = Helpers.resourcesPath('beats') + '/' + beat.uuid + '_trim.' + beat.ext;
			
			ffmpeg(path).inputOptions('-t 10').output(trimPath).run();
			
			ffmpeg.ffprobe(path, async (err, metadata) => {
				try {
					beat.duration = Math.round(metadata.format.duration);
					
					beat.bpm = metadata.format.tags.TBPM;
				} catch (e) {
				
				} finally {
					await beat.save();
				}
			});
			
			return response.send({
				status: true,
				beat,
			})
		}
		response.send({
			status: false,
		});
	}
	
	async download({params, response, auth}) {
		const user = auth.user;
		const beat = await Beat.findBy('uuid', params.uuid);
		let path = Helpers.resourcesPath('beats/' + beat.uuid + '.' + beat.ext);
		
		let bought = false;
		
		if (user != null) {
			const orders = await user.orders().fetch();
			
			for (let order of orders.rows) {
				if (order.status !== 1) continue;
				
				let beats = await order.beats().fetch();
				beats = beats.rows;
				if (beats.some(bt => bt.id === beat.id)) {
					bought = true;
					break;
				}
			}
			
		}
		if (!bought) {
			path = Helpers.resourcesPath('beats/' + beat.uuid + '_trim.' + beat.ext);
		}
		return response.download(path);
	}
	
}

module.exports = BeatController;
