'use strict';

const User = use('App/Models/User');
const Hash = use('Hash');

class UserController {
	
	async login({request, response, auth}) {
		const data = request.only(['mail', 'password']);
		const mail = data.mail;
		const password = data.password;
		try {
			const account = await auth.attempt(mail, password);
			const token = await account.jwt.token;
			response.send({
				status: true,
				token: token,
				admin: account.user.admin,
			})
		} catch (e) {
			response.send({
				status: false,
				message: 'Invalid credentials',
			})
		}
	}
	
	async register({request, response, auth}) {
		const data = request.only(['mail', 'password']);
		const mail = data.mail;
		const password = data.password;
		
		const existUser = await User.findBy('email', mail);
		if (existUser != null) {
			return response.send({
				status: false,
				message: 'User already exist',
			});
		}
		
		const user = new User();
		user.email = mail;
		user.password = password;
		await user.save();
		const authenticated = await auth.attempt(mail, password);
		const token = authenticated.token;
		console.log(token);
		return response.send({
			status: true,
			token: token,
			admin: user.admin
		})
	}
	
	async orders({auth, response}) {
		const user = auth.user;
		let orders = await user.orders().fetch();
		orders = orders.rows;
		
		const allBeats = [];
		
		for (const order of orders) {
			if (order.status !== 1) continue;
			let beats = await order.beats().fetch();
			beats = beats.rows;
			beats.forEach(beat => allBeats.push(beat))
		}
		
		return allBeats;
	}
}

module.exports = UserController;
