'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */

/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Except = use('@adonisjs/generic-exceptions');

class Administrator {
	/**
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Function} next
	 */
	async handle({request, auth}, next) {
		//console.log(auth.user);
		if (auth.user !== null && !auth.user.admin) {
			throw new Except.HttpException('Only Administrators are allowed', 403, 'E_ADMIN_ONLY');
		}
		await next()
	}
}

module.exports = Administrator;
