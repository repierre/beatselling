'use strict';

const Http = exports = module.exports = {};

/**
 * handle errors occured during a Http request.
 *
 * @param  {Object} error
 * @param  {Object} request
 * @param  {Object} response
 */
Http.handleError = function* (error, request, response) {
	/*response.send({
		status: false,
		error: {
			code: error,
		}
	})*/
	response.status(200).send({status: false, error});
};
