import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter);

const routes = [
	{
		path: '/login',
		name: 'login',
		component: () => import('../views/Login.vue')
	},
	{
		path: '/register',
		name: 'register',
		component: () => import('../views/Register.vue')
	},
	{
		path: '/',
		name: 'home',
		component: Home
	},
	{
		path: '/beats',
		name: 'beats',
		component: () => import('../views/Beats.vue')
	},
	{
		path: '/profile',
		name: 'profile',
		component: () => import('../views/Profile')
	},
	{
		path: '/cart',
		name: 'cart',
		component: () => import('../views/Cart')
	},
	{
		path: '/panel',
		name: 'administration',
		component: () => import('../views/Administation'),
		children: [
			{
				path: 'beats',
				component: () => import('../components/panel/Beats'),
				name: 'panel.beats'
			}, {
				path: 'promotions',
				component: () => import('../components/panel/Promotions'),
				name: 'panel.promotions'
			}, {
				path: 'accounts',
				component: () => import('../components/panel/Beats'),
				name: 'panel.accounts'
			}, {
				path: 'stats',
				component: () => import('../components/panel/Beats'),
				name: 'panel.stats'
			},
		]
	}
];

const publicRoutes = [
	'login', 'register', 'beats', 'home',
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
});

router.beforeEach((to, from, next) => {
	const loggedIn = localStorage.getItem('token');
	
	if (to.name === 'login' || to.name === 'register') if (loggedIn) return next('/');
	if (!publicRoutes.includes(to.name) && !loggedIn) return next('/login');
	next();
});

export default router
