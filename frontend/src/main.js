import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

import auth from './api/auth';
import {library} from "@fortawesome/fontawesome-svg-core";
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';

import {fab} from '@fortawesome/free-brands-svg-icons';
import RadialProgressBar from 'vue-radial-progress';
import vuetify from './plugins/vuetify';
import '../node_modules/material-design-icons-iconfont/dist/material-design-icons.css';

library.add(fas);
library.add(far);
library.add(fab);

Vue.component('icon', FontAwesomeIcon);
Vue.component('progressbar', RadialProgressBar);

Vue.config.productionTip = false;

const authHelper = {
	install(Vue, options) {
		Vue.prototype.$auth = auth;
	}
};

Vue.use(authHelper);


new Vue({
	router,
	store,
	vuetify,
	render: h => h(App)
}).$mount('#app');
