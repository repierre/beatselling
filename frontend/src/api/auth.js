import router from '../router';

export default {
	
	isLogged: () => {
		return localStorage.getItem('token') !== null;
	},
	
	isAdmin: () => {
		return localStorage.getItem('admin') === '1';
	},
	
	logout: () => {
		localStorage.removeItem('token');
		localStorage.removeItem('admin');
		router.go();
	},
	
	request: function (endpoint, options = {}) {
		if (options.headers === undefined) options.headers = {};
		if (options.headers['Authorization'] === undefined && this.isLogged()) {
			options.headers['Authorization'] = 'Bearer' +
					' ' + localStorage.getItem('token');
		}
		if (options.headers['Content-Type'] === null) options.headers['Content-Type'] = 'application/json';
		if (options.body !== undefined) {
			if (options.body.constructor === Object) options.body = JSON.stringify(options.body);
		}
		return fetch(process.env.VUE_APP_API_URL + endpoint, options).catch(e => {
			this.logout();
		}).then(data => {
			if (data.status !== 200) {
				this.logout();
			}
			return data;
		})
	}
	
};
