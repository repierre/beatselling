import auth from './auth'

export default {
	
	getProds: function (callback) {
		
		const prods = auth.request('beats/list', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(data => data.json()).then(json => callback(json));
	},
	
	buy: function (prods, callback) {
		const items = [];
		prods.forEach(prod => {items.push({id: prod.id})});
		auth.request( + 'buy', {
			method: 'POST',
			body: JSON.stringify({
				items: items
			})
		}).then(data => data.json()
		).then(json => {
			callback(json);
		});
	}
	
}
