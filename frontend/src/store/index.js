import Vue from 'vue'
import Vuex from 'vuex'
import prods from "./prods";
import cart from "./cart";

Vue.use(Vuex);

const store = new Vuex.Store({
	modules: {
		cart: cart,
		prods: prods
	},
});


export default store;
