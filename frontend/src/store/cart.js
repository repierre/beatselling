import api from "../api/prods";

const state = {
	items: []
};

const getters = {
	cartTracks: (state, getters, rootState) => {
		return state.items.map(({id}) => {
			const track = rootState.prods.all.find(product => product.id === id);
			return {
				id: track.id,
				name: track.name,
				price: track.price
			}
		})
	},
	totalPrice: (state, getters) => {
		return getters.cartTracks.reduce((total, prod) => {
			return total + prod.price;
		}, 0);
	},
};

const actions = {
	buy({commit, state}, prods) {
		const cartItems = [...state.items];
		api.buy(prods, (response) => {
			console.log(response);
		})
	},
	addProd({state, commit}, prod) {
		commit('addToCart', prod.id)
	},
	removeProd({state, commit}, prod) {
		commit('removeFromCart', prod.id)
	},
};


const mutations = {
	addToCart(state, id) {
		state.items.push({
			id
		})
	},
	removeFromCart(state, id) {
		for (let i = 0; i < state.items.length; i++) {
			if (state.items[i].id === id) {
				state.items.splice(i, 1);
				break;
			}
		}
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}
