import api from "../api/prods";

const state = {
	all: []
};

const getters = {};

const actions = {
	getAllProds ({ commit }) {
		api.getProds(products => {
			commit('setProds', products)
		})
	}
};


const mutations = {
	setProds(state, prods) {
		state.all = prods
	},
};

export default {
	namespace: true,
	state, getters, actions, mutations
}
